# Image labels - v 1.0.0

## Introduction

This standard is not a general docker standard, but a Jitesoft organization standard for labeling docker images.  
Standards are always a good idea, as ad-hock labeling will create a confusing system for labeling images.

## Standard

### Maintainer

The commonly used maintainer labels in jitesoft images are the following:

```
maintainer="FULL_NAME <FIRST_NAME@jitesoft.com>"
maintainer.org="Jitesoft"
maintainer.org.uri="https://jitesoft.com"
```

As seen, this is a top-level label, which we really try not to use, but the maintainer label is a standard that is already used.

### Project

The project labels are directly connected to the project, that is, repo uri, issues uri, registry uri etc.  
The labels are defined as following:

```
com.jitesoft.project.repo.type="git"
com.jitesoft.project.repo.uri="URI_TO_REPOSITORY"
com.jitesoft.project.repo.issues="URI_TO_REPOSITORY_ISSUES"
com.jitesoft.project.registry.uri="URI_TO_IMAGE_REGISTRY(FULL)"
```

More labels can be added if wanted.

### Build

The build labels are used to specify specific build types like arch etc.  
As with project labels, they use the `com.jitesoft` namespace.

```
com.jitesoft.build.arch="x64"
```

### App specific

When the image is intended for a specific app, the app labels are used, they use the `com.jitesoft` namespace as with the others:

```
com.jitesoft.app.alpine.version="3.9.4"
com.jitesoft.app.php.version="7.3.2"
com.jitesoft.app.composer.version="1.8.6"
```
