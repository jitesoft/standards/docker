# Docker standards

This repository contains a few small dockerfile standards used by the Jitesoft organization.  
This is a soft standard, but should be kept as much as possible within the organization at the least.

* [`dockerfile`](dockerfile.md)
* `labels`
  * [`images`](labels/images.md)
