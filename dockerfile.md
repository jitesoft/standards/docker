# Jitesoft Dockerfile standard - v 1.0.0

## Args before FROM

Arguments should be placed as early in the build file as possible. If the
image is intended to be built by CI, they do not require a default value, while
if they are not intended for CI, the value may be set.

The `FROM` should be placed right after the arguments (new line may be used).

The arguments should be placed first, so that they can be used in the `FROM` line. 
While the argument in `FROM` is not a requirement, the standard is to keep it easier
to read the dockerfile.

```dockerfile
ARG ALPINE_VERSION="3.9.4"
ARG PHP_VERSION="7.3.6"
FROM registry.gitlab.com/jitesoft/dockerfiles/alpine:${ALPINE_VERSION}
# Rest of docker file.
```

## Labels

Labels should be one declaration separated by `\` and new line (`LF`), they may use `ARGS`.

```dockerfile
ARG ALPINE_VERSION="3.9.4"
ARG PHP_VERSION="7.3.6"
FROM registry.gitlab.com/jitesoft/dockerfiles/alpine:${ALPINE_VERSION}

LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>" \
      ...
      com.jitesoft.app.php.version="${PHP_VERSION}"
```

See [`labels/images`](labels/images.md) for more in-depth label specification.

## Env

If possible, all the `ENV` variables should be declared as with the labels, each 
should be on its own line and be separated by `\` and new line (`LF`).

Env variables should have a default value, which may be set by the `ARGS`.

```dockerfile
ARG ALPINE_VERSION="3.9.4"
ARG PHP_VERSION="7.3.6"
ARG EXTRA_PHP_ARGS
FROM registry.gitlab.com/jitesoft/dockerfiles/alpine:${ALPINE_VERSION}

LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>" \
      ...
      com.jitesoft.app.php.version="${PHP_VERSION}"
      
ENV PHP_INI_DIR="/usr/local/etc/php" \
    PHP_EXTRA_CONFIGURE_ARGS="${EXTRA_PHP_ARGS}"
```

## Copy/Add

As copy/add directives are cache breaking if the file they use is changed, the
directives should if possible be used after heavy operations, but if they are needed 
by the build script, they should go after the `ENV` declarations.

## RUN

Try to keep the `RUN` declarations as few as possible, use `sh` as `SHELL` if
it's not absolutely required to use something else. Each line is separated by
a `\` and a new line (`LF`) where the following line is started with a space, then
`&&` and then another space, before the next command is invoked.

Multi line commands should be aligned with the above command.
Intendation uses `2 space - no tab` rule and multi line commands with clauses (like if/for etc)
should look like following:

```dockerfile
RUN mkdir test/ \
 && if [[ ... ]]; do \
      curl -sSL https://jitesoft.com/downloads/cool.zip -o file.zip \
    fi \
 && mv file.zip test/file.zip
```

## Entrypoint

The entrypoint directive should invoke a `entrypoint` file if used, the entrypoint 
file could basically just invoke the `CMD` if wanted, but should be there to
make it possible to implement extra stuff at a later point.

It is allowed to switch shell when invoking the entrypoint if wanted.

Always use `JSON/ARRAY` type of syntax for entrypoint:

```dockerfile
ENTRYPOINT ["entrypoint"]
```

## CMD

The `CMD` directive should call on the application to invoke when a container is created
the `CMD` will go through the entrypoint script, so additional arguments could be used.

As with entrypoints, always use `JSON/ARRAY` type of syntax for the CMD:

```dockerfile
CMD ["php", "-r", "eval('hax');"]
```

## Additional

If the image contains an application which is long running, use `HEALTHCHECK` directives.

The directive should point to a `healthcheck` shell script either placed in the root dir or 
in the `/usr/local/bin` (alt `/usr/bin`) directory and be invoked with a decent interval.

```dockerfile
HEALTHCHECK --interval=2m --timeout=5s CMD /healthcheck
# alt
HEALTHCHECK --interval=2m --timeout=5s CMD healthcheck
```

A simple healthcheck file could look like the following:

```bash
#!/bin/sh
wget -O/dev/null 127.0.0.1:80 || exit 1
```

But have to be fitting for the specific application running.
